#! /usr/bin/env dcli

import 'dart:io';
import 'dart:async';

import 'package:dcli/dcli.dart';

final String cache = '$HOME/.cache/publicip';

Future<void> main(List<String> args) async {
  var parser = ArgParser();
  parser.addFlag('verbose', abbr: 'v', defaultsTo: false, negatable: false);
  parser.addFlag('public', abbr: 'p', negatable: true, defaultsTo: true);
  parser.addFlag('force', abbr: 'f', negatable: true, defaultsTo: false);
  parser.addOption('local', abbr: 'l');
  var results = parser.parse(args);
  var force = results['force'] as bool;

  if (results.wasParsed('verbose')) Settings().setVerbose(enabled: true);

  if (results['local'] != null) {
    print(await getLocalIp(results['local'] as String));
    exit(0);
  }

  if (!exists(cache)) {
    touch(cache, create: true);
    force = true;
  }

  var file = File(cache);
  var modified = file.lastModifiedSync();
  if (force || modified.compareTo(DateTime.now().subtract(Duration(days: 1))) < 0) {
    try {
      var publicip = 'curl -s https://api.ipify.org'.firstLine;
      file.writeAsStringSync(publicip);
    } on Exception catch (e) {
      print('');
      exit(1);
    }
  }

  print(read(cache).firstLine);
  // 'dig +short myip.opendns.com @resolver1.opendns.com'.run;
}

Future<String> getLocalIp(String eth) async {
  return (await NetworkInterface.list())
      .firstWhere((interface) => interface.name == eth, orElse: () => null)
      ?.addresses
      ?.first
      ?.address;
}
